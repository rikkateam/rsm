namespace Rsm.Generators
{
    /// <summary>
    /// ��������� ��� ���������� sql. ��������� ������� ������� �� ��������� �������
    /// </summary>
    public interface ISqlGenerator
    {
        /// <summary>
        /// ��������� ������ ��� �������� �������
        /// </summary>
        /// <param name="modelName">������ ��� ������� ��������� ������</param>
        /// <returns></returns>
        string TableCreate(string modelName);

        /// <summary>
        /// ��������� ��������� �������
        /// </summary>
        /// <param name="modelName">������ ��� ������� ��������� ������</param>
        /// <returns></returns>
        string TableAlter(string modelName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelName">������ ��� ������� ��������� ������</param>
        /// <returns></returns>
        string TableDrop(string modelName);

        /// <summary>
        /// �������� ������� ��� �������� �������
        /// </summary>
        /// <param name="modelName">������ ��� ������� ��������� ������</param>
        /// <param name="properties">���� ������� ����� �������</param>
        /// <returns></returns>
        string RowInsert(string modelName, string[] properties);

        /// <summary>
        /// �������� ������� ��������� �����
        /// </summary>
        /// <param name="modelName">������ ��� ������� ��������� ������</param>
        /// <param name="properties">���� ��� ������� ���������� ���������</param>
        /// <returns></returns>
        string RowUpdate(string modelName, string[] properties);

        /// <summary>
        /// ������� ����� ������
        /// </summary>
        /// <param name="modelName">������ ��� ������� ��������� ������</param>
        /// <param name="properties">���� ��� ������� ���� �������</param>
        /// <param name="filters">������� �������</param>
        /// <returns></returns>
        string RowSelectOne(string modelName, string[] properties, Filter[] filters);

        /// <summary>
        /// ������� ��������� �����
        /// </summary>
        /// <param name="modelName">������ ��� ������� ��������� ������</param>
        /// <param name="properties">���� ��� ������� ���� �������</param>
        /// <param name="filters">������� �������</param>
        /// <returns></returns>
        string RowSelectMany(string modelName, string[] properties, Filter[] filters);

        /// <summary>
        /// �������� ������� ��� �������� ������ �������
        /// </summary>
        /// <param name="modelName">������ ��� ������� ��������� ������</param>
        /// <returns></returns>
        string RowDelete(string modelName);
    }

    public class Filter
    {
        public string Property { get; set; }
        public CondOperator Operator { get; set; }
        public string OperatorRigthOperant { get; set; }
    }
}