﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Rsm.TypeProviders;

namespace Rsm.Generators
{
    /// <summary>
    /// Класс реализующий <see cref="ISqlGenerator"/>. Генератор создает скрипты по указанным методам
    /// </summary>
    public class MsSqlGenerator : ISqlGenerator
    {

        /// <summary>
        /// соответствие обычных типов с типами SQL Server
        /// </summary>
        private Dictionary<string, string> _basicTypeMap;

        /// <summary>
        /// Пространство для которого создаются скрипты
        /// </summary>
        private readonly RsmNamespace _rsmNamespace;

        /// <summary>
        /// Провайдеры простых типов
        /// </summary>
        private readonly Dictionary<string, ISpecialBasciTypeProvider> _basicTypeProviders;

        /// <summary>
        /// Провайдеры ссылочных типов
        /// </summary>
        private readonly Dictionary<string, ISpecialRelationTypeProvider> _relationTypeProviders;

        /// <summary>
        /// соединение с базой
        /// </summary>
        private readonly SqlConnection _connection;

        /// <summary>
        /// имя свойства для идентификатора
        /// </summary>
        private const string RowIdProperty = "id";

        public MsSqlGenerator(RsmNamespace rsmNamespace,Dictionary<string, ISpecialBasciTypeProvider> basicTypeProviders,Dictionary<string, ISpecialRelationTypeProvider> relationTypeProviders,SqlConnection connection)
        {
            _rsmNamespace = rsmNamespace;
            _basicTypeProviders = basicTypeProviders;
            _relationTypeProviders = relationTypeProviders;
            _connection = connection;
            _basicTypeMap = new Dictionary<string, string>()
            {
                {"String","nvarchar(max)" },
                {"String (50)","nvarchar(50)" },
                {"String (100)","nvarchar(100)" },
                {"String (250)","nvarchar(250)" },
                {"Int","int" },
                {"Decimal","decimal(18,0)" },
                {"Boolean","bit" },
                {"DateTime","datetime" },
                {"Uuid","uniqueidentifier" },
            };
        }

        /// <summary>
        /// создание имени для таблицы, чтобы при выполнении скрипты не было конфликтов с другими
        /// </summary>
        /// <param name="name">предполагаемое название</param>
        /// <returns></returns>
        private string _generateTableName(string name)
        {
            var generated = string.IsNullOrEmpty(_rsmNamespace.Prefix) ? name : _rsmNamespace.Prefix + "_" + name;
            //запрещенные названия которые должны быть изменены
            var banned = new string[] { "Files", "Roles", "Users" };
            var first = false;
            while (!first)
            {
                if (banned.Any(a => a == name))
                {
                    const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    var r = new Random(DateTime.Now.Millisecond);
                    generated = string.IsNullOrEmpty(_rsmNamespace.Prefix) ? name : _rsmNamespace.Prefix + "_" + name;
                    generated += generated[r.Next()] + generated[r.Next(chars.Length - 1)] + generated[r.Next(chars.Length - 1)] + generated[r.Next(chars.Length - 1)];
                    if (!_checkTableName(generated))
                        continue;
                }                
                first = true;
            }
            //создание цифры
            if (!_checkTableName(generated))
            {
                for (var i = 1; i < int.MaxValue; i++)
                {
                    if (_checkTableName(generated+i))
                    {
                        generated += i;
                        break;
                    }
                }
            }
            return generated;
        }

        /// <summary>
        /// проверка существовани таблицы с заданным именем
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private bool _checkTableName(string name)
        {
            var sql = $@"SELECT TABLE_NAME
            FROM INFORMATION_SCHEMA.TABLES
            WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_CATALOG = '{_connection.Database}'";
            var tables = _connection.Query<string>(sql);
            return tables.All(a => a != name);
        }

        /// <summary>
        /// создается скрипт для создания таблицы
        /// </summary>
        /// <param name="modelName">модель для которой создается скрипт</param>
        /// <returns></returns>
        public string TableCreate(string modelName)
        {
            var pre = new Dictionary<string, string>();
            var post = new Dictionary<string, string>();
            var properties = new Dictionary<string,string>();
            var constraints = new Dictionary<string, string>();
            var singleProcessed = new List<string>();
            var multiplyProcessed = new List<string>();
            var model = _rsmNamespace.DataModels.FirstOrDefault(s => s.SystemName == modelName);
            if(model==null)
                throw new Exception();
            if(string.IsNullOrEmpty(model.TableName))
                model.TableName = _generateTableName(model.SystemName);
            
            // множественные вхождения
            #region multiply
            var multiplyProperties =
                model.RuntimeProperties.Where(w => w.Multiple == true).OrderBy(o => o.SystemName).ToArray();
            var singleProperties = model.RuntimeProperties.Where(w => w.Multiple == false).OrderBy(o => o.SystemName).ToList();
            

            foreach (var prop in multiplyProperties.Where(p => _basicTypeMap.ContainsKey(p.Type)))
            {
                //обычный тип mssql
                if(string.IsNullOrEmpty(prop.MultipleTableName))
                    prop.MultipleTableName = _generateTableName(model.SystemName + "_" + prop.SystemName);
                var command = $"CREATE TABLE [{_rsmNamespace.Schema}].[{prop.MultipleTableName}]([{RowIdProperty}] uniqueidentifier NOT NULL PRIMARY KEY,[{prop.SystemName}] {_basicTypeMap[prop.Type]} NOT NULL)";
                pre.Add(prop.SystemName,command);
                multiplyProcessed.Add(prop.SystemName);
            }
            foreach (var prop in multiplyProperties.Where(p => _basicTypeProviders.ContainsKey(p.Type)))
            {
                //специальный тип
                if (string.IsNullOrEmpty(prop.MultipleTableName))
                    prop.MultipleTableName = _generateTableName(model.SystemName + "_" + prop.SystemName);
                var command = $"CREATE TABLE [{_rsmNamespace.Schema}].[{prop.MultipleTableName}]([{RowIdProperty}] uniqueidentifier NOT NULL PRIMARY KEY,[{prop.SystemName}] {_basicTypeProviders[prop.Type]} NOT NULL)";
                pre.Add(prop.SystemName, command);
                multiplyProcessed.Add(prop.SystemName);
            }
            foreach (var prop in multiplyProperties.Where(p => _relationTypeProviders.ContainsKey(p.Type)))
            {
                //ссылка на специальный тип, значения в другой таблице
                if (string.IsNullOrEmpty(prop.MultipleTableName))
                    prop.MultipleTableName = _generateTableName(model.SystemName + "_" + prop.SystemName);
                var pre1 = $"CREATE TABLE [{_rsmNamespace.Schema}].[{prop.MultipleTableName}]([{RowIdProperty}] uniqueidentifier NOT NULL PRIMARY KEY,[{prop.SystemName}] uniqueidentifier NOT NULL)";
                pre.Add(prop.SystemName, pre1);
                multiplyProcessed.Add(prop.SystemName);
            }
            foreach (var prop in multiplyProperties.Where(p => multiplyProcessed.All(a=>a!=p.SystemName)))
            {
                //ссылка на пользовательский тип, значения в другой таблице
                if (string.IsNullOrEmpty(prop.MultipleTableName))
                    prop.MultipleTableName = _generateTableName(model.SystemName + "_" + prop.SystemName);
                var reference = _rsmNamespace.DataModels.FirstOrDefault(f => f.SystemName == prop.Type);
                if (reference == null)
                    throw new Exception();

                var pre1 = $"CREATE TABLE [{_rsmNamespace.Schema}].[{prop.MultipleTableName}]([{RowIdProperty}] uniqueidentifier NOT NULL PRIMARY KEY,[{prop.SystemName}] uniqueidentifier NOT NULL)";
                pre.Add(prop.SystemName, pre1);
            }
            #endregion

            // одиночные вхождения
            #region single
            foreach (var prop in singleProperties.Where(p => _basicTypeMap.ContainsKey(p.Type)))
            {
                //обычный тип mssql
                var sql = $"[{prop.SystemName}] {_basicTypeMap[prop.Type]}";
                sql += prop.Required ? " NOT NULL" : " NULL";
                if (prop.SystemName == "id")
                    sql += " PRIMARY KEY";
                if (!string.IsNullOrEmpty(prop.DefaultValue))
                {
                    sql += $" DEFAULT({prop.DefaultValue})";
                }
                properties.Add(prop.SystemName,sql);
                singleProcessed.Add(prop.SystemName);
            }
            foreach (var prop in singleProperties.Where(p => _basicTypeProviders.ContainsKey(p.Type)))
            {
                //специальный тип
                var sql = $"[{prop.SystemName}] {_basicTypeProviders[prop.Type]}";
                sql += prop.Required ? " NOT NULL" : " NULL";
                properties.Add(prop.SystemName, sql);
                singleProcessed.Add(prop.SystemName);
            }
            foreach (var prop in singleProperties.Where(p => _relationTypeProviders.ContainsKey(p.Type)))
            {
                //ссылка на специальный тип, значения в другой таблице
                var sql = $"[{prop.SystemName}] {_relationTypeProviders[prop.Type]}";
                properties.Add(prop.SystemName, sql);
                singleProcessed.Add(prop.SystemName);
            }
            foreach (var prop in singleProperties.Where(p => singleProcessed.All(a=>a!=p.SystemName)))
            {
                //ссылка на пользовательский тип, значения в другой таблице

                var sql = $"[{prop.SystemName}] uniqueidentifier";
                sql += prop.Required ? " NOT NULL" : " NULL";
                properties.Add(prop.SystemName, sql);
                var reference = _rsmNamespace.DataModels.FirstOrDefault(f => f.SystemName == prop.Type);
                if (reference == null)
                    throw new Exception();
                if (string.IsNullOrEmpty(reference.TableName))
                    reference.TableName = _generateTableName(reference.SystemName);
                if (string.IsNullOrEmpty(prop.TypeRelationName))
                    prop.TypeRelationName = $"{model.TableName}_{prop.SystemName}_{reference.TableName}_FK";
                var constraintSql = $"CONSTRAINT [{prop.TypeRelationName}] FOREIGN KEY ([{prop.SystemName}]) REFERENCES [{_rsmNamespace.Schema}].[{reference.TableName}]([{RowIdProperty}])";
                if (prop.Cascade)
                {
                    constraintSql += " ON DELETE CASCADE ON UPDATE CASCADE";
                }
                constraints.Add(prop.SystemName, constraintSql);
                //TODO: для каскадных создавать отдельную таблицу
            }
            #endregion
            #region all            
            foreach (var prop in model.RuntimeProperties.Where(p => p.Index && p.Multiple==false &&p.SystemName!="id"))
            {
                if(string.IsNullOrEmpty(prop.IndexName))
                    prop.IndexName = $"IX_{model.TableName}_{prop.SystemName}";
                var command = $"CREATE INDEX {prop.IndexName} ON [{_rsmNamespace.Schema}].[{model.TableName}]({prop.SystemName});";
                post.Add(prop.SystemName, command);
            }
            foreach (var prop in model.RuntimeProperties.Where(p => p.Unique && p.Multiple == false && p.SystemName != "id"))
            {
                if(string.IsNullOrEmpty(prop.UniqueName))
                    prop.UniqueName = $"AK_{model.TableName}_{prop.SystemName}";
                var command = $"CONSTRAINT {prop.UniqueName} UNIQUE({prop.SystemName})";
                constraints.Add(prop.SystemName, command);
            }
            #endregion

            var propertiesSql = string.Join(",", properties.Select(s => s.Value));
            var constraintsSql = string.Join(",", constraints.Select(s => s.Value));
            var comment = $@"-- RsmSqlGenerator geneated at {DateTime.Now.ToString("u")}
--section {model.SystemName}";            
            var create = $"--{model.SystemName}\r\nCREATE TABLE [{_rsmNamespace.Schema}].[{model.TableName}]({propertiesSql}, {constraintsSql});";
            var preSql = string.Join("\r\n", pre.Select(s => s.Value));
            var postSql = string.Join("\r\n", post.Select(s => s.Value));
            var result = $"{comment}\r\n\r\n{preSql}\r\n\r\n{create}\r\n\r\n{postSql}\r\n\r\n--endsection";
            return result;
        }

        /// <summary>
        /// изменение структуры таблицы
        /// </summary>
        /// <param name="modelName">модель для которой создается скрипт</param>
        /// <returns></returns>
        public string TableAlter(string modelName)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelName">модель для которой создается скрипт</param>
        /// <returns></returns>
        public string TableDrop(string modelName)
        {
            var model = _rsmNamespace.DataModels.FirstOrDefault(s => s.SystemName == modelName);
            if (model == null)
                throw new Exception();
            var tableDropSql = $"DROP TABLE [{_rsmNamespace.Schema}].[{model.TableName}];";
            return tableDropSql;
        }

        /// <summary>
        /// Создание скрипта для удаления таблица
        /// </summary>
        /// <param name="modelName">модель для которой создается скрипт</param>
        /// <param name="properties">поля которые будут восится</param>
        /// <returns></returns>
        public string RowInsert(string modelName, string[] properties)
        {
            var model = _rsmNamespace.DataModels.FirstOrDefault(s => s.SystemName == modelName);
            if (model == null)
                throw new Exception();
            var propsDeclare = string.Join(", ", properties.Select(x => "[" + x + "]"));
            var propsValue= string.Join(", ", properties.Select(x => "@"+x));
            var rowInsertSql = "INSERT INTO ["+ _rsmNamespace.Schema+"].["+ model.TableName+"]("+ propsDeclare+") VALUES("+ propsValue + ");";
            return rowInsertSql;
        }

        /// <summary>
        /// Создание скрипта обновения полей
        /// </summary>
        /// <param name="modelName">модель для которой создается скрипт</param>
        /// <param name="properties">поля для которых необходимо обновение</param>
        /// <returns></returns>
        public string RowUpdate(string modelName, string[] properties)
        {
            var model = _rsmNamespace.DataModels.FirstOrDefault(s => s.SystemName == modelName);
            if (model == null)
                throw new Exception();
            var propsValue = string.Join(", ", properties.Select(x => "[" + x + "] = @" + x));
            var rowInsertSql = "UPDATE [" + _rsmNamespace.Schema + "].[" + model.TableName + "] SET " + propsValue + ";";
            return rowInsertSql;
        }

        /// <summary>
        /// Выборка одной строки
        /// </summary>
        /// <param name="modelName">модель для которой создается скрипт</param>
        /// <param name="properties">Поля для которых идет выборка</param>
        /// <param name="filters">Условия выборки</param>
        /// <returns></returns>
        public string RowSelectOne(string modelName, string[] properties, Filter[] filters)
        {
            var model = _rsmNamespace.DataModels.FirstOrDefault(s => s.SystemName == modelName);
            if (model == null)
                throw new Exception();
            var propsValue = string.Join(", ", properties.Select(x => "[" + x + "]"));
            var cond = _getCond(modelName, filters);
            var result = "SELECT TOP(1) "+ propsValue+" FROM [" + _rsmNamespace.Schema + "].[" + model.TableName + "]";
            if (!string.IsNullOrEmpty(cond))
                result += cond + ";";
            else result += ";";
            return result;
        }

        /// <summary>
        /// Выборка множества строк
        /// </summary>
        /// <param name="modelName">модель для которой создается скрипт</param>
        /// <param name="properties">Поля для которых идет выборка</param>
        /// <param name="filters">Условия выборки</param>
        /// <returns></returns>
        public string RowSelectMany(string modelName, string[] properties, Filter[] filters)
        {
            var model = _rsmNamespace.DataModels.FirstOrDefault(s => s.SystemName == modelName);
            if (model == null)
                throw new Exception();
            var propsValue = string.Join(", ", properties.Select(x => "[" + x + "]"));
            var cond = _getCond(modelName, filters);
            var result = "SELECT TOP(1) " + propsValue + " FROM [" + _rsmNamespace.Schema + "].[" + model.TableName + "]";
            if (!string.IsNullOrEmpty(cond))
                result += cond + ";";
            else result += ";";
            return result;
        }

        /// <summary>
        /// Создание скрипта для удаления данных таблицы
        /// </summary>
        /// <param name="modelName">модель для которой создается скрипт</param>
        /// <returns></returns>
        public string RowDelete(string modelName)
        {
            var model = _rsmNamespace.DataModels.FirstOrDefault(s => s.SystemName == modelName);
            if (model == null)
                throw new Exception();
            var result = $"DELETE FROM [{_rsmNamespace.Schema}].[{model.TableName}] WHERE [{RowIdProperty}]=@Id";
            return result;
        }

        private string _getCond(string modelName, Filter[] filters)
        {
            var conds = new List<string>();
            foreach (var filter in filters)
            {
                switch (filter.Operator)
                {
                    case CondOperator.Empty:
                        conds.Add("([" + filter.Property + "] IS NULL)");
                        break;
                    case CondOperator.NoEmpty:
                        conds.Add("([" + filter.Property + "] IS NOT NULL)");
                        break;
                    case CondOperator.Equally:
                        conds.Add("([" + filter.Property + "] = @" + filter.OperatorRigthOperant + ")");
                        break;
                    case CondOperator.NoEqually:
                        conds.Add("([" + filter.Property + "] <> @" + filter.OperatorRigthOperant + ")");
                        break;
                    case CondOperator.More:
                        conds.Add("([" + filter.Property + "] > @" + filter.OperatorRigthOperant + ")");
                        break;
                    case CondOperator.Less:
                        conds.Add("([" + filter.Property + "] < @" + filter.OperatorRigthOperant + ")");
                        break;
                    case CondOperator.GreaterEqual:
                        conds.Add("([" + filter.Property + "] >= @" + filter.OperatorRigthOperant + ")");
                        break;
                    case CondOperator.LessEqual:
                        conds.Add("([" + filter.Property + "] <= @" + filter.OperatorRigthOperant + ")");
                        break;
                    case CondOperator.Contains:
                        conds.Add("([" + filter.Property + "] LIKE N'%'+@" + filter.OperatorRigthOperant + "+N'%')");
                        break;
                    case CondOperator.NoContains:
                        conds.Add("([" + filter.Property + "] NOT LIKE N'%'+@" + filter.OperatorRigthOperant + "+N'%')");
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            return string.Join(" AND ", conds);
        }

    }
}
