﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using Dapper;
using Rsm.Generators;
using Rsm.TypeProviders;

namespace Rsm
{
    //public interface IDataProvider
    //{

    //    void Insert(DataModel model, IDictionary<string,object> value);
    //    void Update(DataModel model, Guid id, IDictionary<string,object> value);

    //    DataModelProxySingle Select(DataModel model, Guid id);
    //    //DataModelProxyMany SelectAllLazy(DataModel model);
    //}

    //public class SqlDataProvider : IDataProvider
    //{
    //    public RsmEngine Core;

    //    public void Insert(DataModel model, IDictionary<string,object> value)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public void Update(DataModel model, Guid id, IDictionary<string,object> value)
    //    {
    //        throw new NotImplementedException();
    //    }

    //    public DataModelProxySingle Select(DataModel model, Guid id)
    //    {
    //        throw new NotImplementedException();
    //        //var item = Core.Connection.Query<ExpandoObject>(model.SelectSqlScript).FirstOrDefault();
    //        //var result = new DataModelProxySingle(model, item, id);
    //        //return result;
    //    }

    //    //public DataModelProxyMany SelectAllLazy(DataModel model)
    //    //{
    //    //    throw new NotImplementedException();
    //    //    //var items = Core.Connection.Query<ExpandoObject>(model.SelectAllLazySqlScript).ToList();
    //    //    //var result = new DataModelProxyMany(model, items);
    //    //    //return result;
    //    //}
    //}


    public class DataExecutor
    {
        public DataExecutor(RsmEngine core, string transactionId)
        {
            Core = core;
            Buffer = core.Models.Keys.ToDictionary(d => d, s => new Dictionary<Guid, DataModelProxySetter>());
            TransactionId = transactionId;
        }
        // [модель]->([транзакция]->(запись))
        public Dictionary<string, Dictionary<Guid, DataModelProxySetter>> Buffer;

        public RsmEngine Core;

        public readonly string TransactionId;

        public Guid Insert(string model, IDictionary<string,object> obj)
        {
            var id = Guid.NewGuid();
            Buffer[model]
                .Add(id, new DataModelProxySetter(Core.Models[model], obj, id, true));
            var watchers = Core.ModelProcess[model];
            if (watchers != null && watchers.Count>0)
            {
                foreach (var watcher in watchers)
                {
                    //просмотр наличия транзации и добавление при отсутствии
                    if (!Core.RsmExecutorTransact.ContainsKey(TransactionId))
                        Core.RsmExecutorTransact.Add(TransactionId, new RsmExecutor(Core, TransactionId));
                    Core.RsmExecutorTransact[TransactionId].UpdateState(watcher, id, obj, TransactionId);
                }
            }
            return id;
        }
        public void Update(string model, Guid id, IDictionary<string,object> obj)
        {
            if (!Buffer[model].ContainsKey(id))
                Buffer[model].Add(id, new DataModelProxySetter(Core.Models[model], obj, id, false));
            var watchers = Core.ModelProcess[model];
            if (watchers != null && watchers.Count > 0)
            {
                foreach (var watcher in watchers)
                {
                    //просмотр наличия транзации и добавление при отсутствии
                    if (!Core.RsmExecutorTransact.ContainsKey(TransactionId))
                        Core.RsmExecutorTransact.Add(TransactionId, new RsmExecutor(Core, TransactionId));
                    Core.RsmExecutorTransact[TransactionId].UpdateState(watcher, id, obj, TransactionId);
                }
            }
        }

        public void WriteChanges()
        {

            foreach (var model in Buffer.Keys)
            {
                var def = Core.Models[model];
                foreach (var data in Buffer[model])
                {
                    if (data.Value.Insert)
                    {
                        var query = Core.Generator.RowInsert(model, data.Value.Object.Keys.ToArray());
                        //TODO:проверить нормально ли принимает dapper такой параметр, 
                        //возможно необходимо использовать DynamicParameters
                        Core.Connection.Execute(query, data.Value.Object);
                    }
                    else
                    {
                        //TODO: генерация скриптов обновления на основе указанных полей
                        throw new NotImplementedException();
                        //Core.Connection.Execute(def.UpdateSqlScript, data.Value);
                    }
                }
            }
        }

        public void Commit()
        {
            //TODO: commit
        }

        public void Cansel()
        {
            Buffer = new Dictionary<string, Dictionary<Guid, DataModelProxySetter>>();
        }
    }
    public class RsmExecutor
    {
        public RsmExecutor(RsmEngine core, string transactionId)
        {
            Core = core;
            TransactionId = transactionId;
            Buffer = core.Proccesses.Keys.ToDictionary(d => d, s => new Dictionary<Guid, RsmProccessInfo>());
        }
        public readonly string TransactionId;

        public RsmEngine Core;

        // [процесс]->([транзакция]->(запись))
        public Dictionary<string, Dictionary<Guid, RsmProccessInfo>> Buffer;

        public void ExecProccessAction(string proccess, Guid id, string action, string transactionId)
        {
            throw new NotImplementedException();
        }

        public void JumpProccess(string proccess, Guid id, string toState, string transactionId)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// реакция на обновление, обновление может быть запущено как реакция системы на действия пользователя или другого процесса
        /// </summary>
        /// <param name="proccess"></param>
        /// <param name="id"></param>
        /// <param name="obj"></param>
        /// <param name="transactionId"></param>
        public void UpdateState(string proccess, Guid id, IDictionary<string, object> obj, string transactionId)
        {
            var def = Core.Proccesses[proccess];
            var proccessInfo = Buffer[proccess];
            if (proccessInfo == null)
            {
                proccessInfo = new Dictionary<Guid, RsmProccessInfo>();
                Buffer.Add(proccess, proccessInfo);
            }
            var cur = proccessInfo[id];
            if (cur == null)
            {
                //TODO: уточнить запрос
                cur = Core.Connection.QueryFirstOrDefault<RsmProccessInfo>("SELECT * FROM [ngn].[Processes] WHERE [DataId] = @Id", new { Id = id });
                if (cur == null)
                {
                    cur = new RsmProccessInfo()
                    {
                        DataId = id,
                        State = def.StartState,
                        Progress = StateProgress.Entering
                    };
                }
                proccessInfo.Add(id, cur);
            }
            var state = def.States.FirstOrDefault(f => f.SystemName == cur.State);
            if (state == null)
            {
                throw new Exception();
            }
            foreach (var script in state.UpdateScripts)
            {
                //TODO: запуск скриптов
            }
            foreach (var transition in state.CondTransitions)
            {
                //TODO: проверка условий
                //TODO: переход состояния
            }
            throw new NotImplementedException("логика смены состояний");
        }

        public void WriteChanges()
        {
            throw new NotImplementedException();
        }

        public void Commit()
        {
            //TODO: commit
        }

        public void Cansel()
        {
            Buffer = new Dictionary<string, Dictionary<Guid, RsmProccessInfo>>();
        }
    }
    public class RsmEngine : RsmEngineBase
    {

        public RsmEngine(RsmNamespace rsmNamespace) : base(rsmNamespace)
        {
            Models = rsmNamespace.DataModels.ToDictionary(d => d.SystemName);

            var baseModels = new BaseModelsContainer();
            var spBasic = new SpecialBasciTypeProviderContainer();
            var spRelation = new SpecialRelationTypeProviderContainer();

            foreach (var model in baseModels.Models)
            {
                Models.Add(model.Key,model.Value);
            }
            foreach (var model in Models.Where(w=> !string.IsNullOrEmpty(w.Value.RootType)))
            {
                _collectRootProperties(model.Value);
            }
            Proccesses = rsmNamespace.Proccesses.ToDictionary(d => d.SystemName);
            RsmExecutorTransact = new Dictionary<string, RsmExecutor>();
            DataExecutorTransact = new Dictionary<string, DataExecutor>();
            ModelProcess = new Dictionary<string, List<string>>();
            Connection = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ConnectionString);            
            Generator = new MsSqlGenerator(rsmNamespace, spBasic.TypeProviders, spRelation.TypeProviders, Connection);
            foreach (var model in rsmNamespace.DataModels)
            {
                var proccess = rsmNamespace.Proccesses.Where(w => w.States.Any(a => a.DataModel == model.SystemName)).Select(s=>s.SystemName).ToArray();
                if (proccess.Length > 0)
                {
                    ModelProcess.Add(model.SystemName, proccess.ToList());
                }
            }
        }

        private void _collectRootProperties(DataModel model)
        {
            model.RuntimeProperties.AddRange(model.Properties);            
            var root = Models[model.RootType];
            while (root != null)
            {
                foreach (var property in root.Properties)
                {
                    model.RuntimeProperties.Add(new DataProperty(property.Name, property.SystemName, property.Type,required: property.Required,index: property.Index,unique: property.Unique,multiple: property.Multiple,cascade: property.Cascade));
                }
                if (string.IsNullOrEmpty(root.RootType))
                    break;
                root = Models[root.RootType];
            }
        }        

        public ISqlGenerator Generator { get; set; }

        //public override async Task Update()
        //{
        //    throw new NotImplementedException();            
        //}

        public SqlConnection Connection;

        ///// <summary>
        ///// функционал для записи данных в базу
        ///// </summary>
        //public IDataProvider Provider;

        /// <summary>
        /// транзакции данных. [транзакция]->(объект транзакции)
        /// </summary>
        public Dictionary<string, DataExecutor> DataExecutorTransact;

        /// <summary>
        /// транзакция процессов. [транзакция]->(объект транзакции)
        /// </summary>
        public Dictionary<string, RsmExecutor> RsmExecutorTransact;

        /// <summary>
        /// модели. [имя модели]->(описание модели)
        /// </summary>
        public Dictionary<string, DataModel> Models;

        /// <summary>
        /// процессы. [имя процесса]->(описание процесса)
        /// </summary>
        public Dictionary<string, RsmProccess> Proccesses;

        /// <summary>
        /// соответствие модели и процесса. // [модель]->([процессы]))
        /// </summary>
        public Dictionary<string, List<string>> ModelProcess;


        /// <summary>
        /// запуск извне для обновления данных
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id"></param>
        /// <param name="obj"></param>
        /// <param name="transactionId"></param>
        public override void UpdateData(string model, Guid id, IDictionary<string, object> obj, string transactionId)
        {
            if(!DataExecutorTransact.ContainsKey(transactionId))
                DataExecutorTransact.Add(transactionId, new DataExecutor(this, transactionId));
            DataExecutorTransact[transactionId].Update(model, id, obj);
        }

        /// <summary>
        /// запуск извне для внесения данных
        /// </summary>
        /// <param name="model"></param>
        /// <param name="obj"></param>
        /// <param name="transactionId"></param>
        /// <returns></returns>
        public override Guid InsertData(string model, IDictionary<string, object> obj, string transactionId)
        {
            if (!DataExecutorTransact.ContainsKey(transactionId))
                DataExecutorTransact.Add(transactionId, new DataExecutor(this, transactionId));
            var id = DataExecutorTransact[transactionId].Insert(model, obj);
            return id;
        }

        /// <summary>
        /// запуск извне для выполнения пользовательского действия
        /// </summary>
        /// <param name="proccess"></param>
        /// <param name="id"></param>
        /// <param name="action"></param>
        /// <param name="transactionId"></param>
        public override void ExecProccessAction(string proccess, Guid id, string action, string transactionId)
        {
            if (!RsmExecutorTransact.ContainsKey(transactionId))
                RsmExecutorTransact.Add(transactionId, new RsmExecutor(this, transactionId));
            RsmExecutorTransact[transactionId].ExecProccessAction(proccess, id, action, transactionId);
        }

        /// <summary>
        /// запуск извне для выполения перехода на другое состояние пользователем
        /// </summary>
        /// <param name="proccess"></param>
        /// <param name="id"></param>
        /// <param name="toState"></param>
        /// <param name="transactionId"></param>
        public override void JumpProccess(string proccess, Guid id, string toState, string transactionId)
        {
            if (!RsmExecutorTransact.ContainsKey(transactionId))
                RsmExecutorTransact.Add(transactionId, new RsmExecutor(this, transactionId));
            RsmExecutorTransact[transactionId].JumpProccess(proccess, id, toState, transactionId);
        }

        public override void Commit(string transactionId)
        {
            //write data
            if (RsmExecutorTransact.ContainsKey(transactionId))
            {
                RsmExecutorTransact[transactionId].WriteChanges();
            }
            if (DataExecutorTransact.ContainsKey(transactionId))
            {
                DataExecutorTransact[transactionId].WriteChanges();
            }

            //commit data
            if (RsmExecutorTransact.ContainsKey(transactionId))
            {
                RsmExecutorTransact[transactionId].Commit();
                RsmExecutorTransact.Remove(transactionId);
            }
            if (DataExecutorTransact.ContainsKey(transactionId))
            {
                DataExecutorTransact[transactionId].Commit();
                DataExecutorTransact.Remove(transactionId);
            }            
        }

        public override void Rollback(string transactionId)
        {
            if (RsmExecutorTransact.ContainsKey(transactionId))
            {
                RsmExecutorTransact[transactionId].Cansel();
                RsmExecutorTransact.Remove(transactionId);
            }
            if (DataExecutorTransact.ContainsKey(transactionId))
            {
                DataExecutorTransact[transactionId].Cansel();
                DataExecutorTransact.Remove(transactionId);
            }
        }
    }

    /// <summary>
    /// базовый класс
    /// </summary>
    public abstract class RsmEngineBase
    {
        protected RsmEngineBase(RsmNamespace rsmNamespace)
        {
            RsmNamespace = rsmNamespace;
        }
        protected RsmNamespace RsmNamespace;

        public abstract void UpdateData(string model, Guid id, IDictionary<string,object> obj, string transactionId);
        public abstract Guid InsertData(string model, IDictionary<string,object> obj, string transactionId);
        public abstract void ExecProccessAction(string proccess, Guid id, string action, string transactionId);
        public abstract void JumpProccess(string proccess, Guid id, string toState, string transactionId);

        public abstract void Commit(string transactionId);
        public abstract void Rollback(string transactionId);

    }

    public abstract class DataModelProxy
    {
        public DataModel ModelDefinition;
    }

    public class DataModelProxySetter : DataModelProxy
    {

        public DataModelProxySetter(DataModel model, IDictionary<string,object> obj, Guid id, bool insert)
        {
            ModelDefinition = model;
            Object = obj;
            Id = id;
            Insert = insert;
        }
        public IDictionary<string,object> Object;
        public Guid Id;
        public bool Insert;
    }
    public class DataModelProxySingle : DataModelProxy
    {
        public DataModelProxySingle(DataModel model, IDictionary<string,object> obj, Guid id)
        {
            ModelDefinition = model;
            Object = obj;
            Id = id;
        }
        public Guid Id { get; set; }
        public IDictionary<string,object> Object;
    }

    public class DataModelProxyMany : DataModelProxy
    {
        public DataModelProxyMany(DataModel model, List<IDictionary<string,object>> obj)
        {
            ModelDefinition = model;
            Object = obj;
        }
        public List<IDictionary<string,object>> Object;
    }

    /// <summary>
    /// структура хранения информации о запущенных процессах
    /// </summary>
    public class RsmProccessInfo
    {
        public RsmProccessInfo()
        {
            
        }

        public RsmProccessInfo(Guid id, string dataModel,Guid? dataId,string state, Guid? error, StateProgress progress)
        {
            Id = id;
            DataModel = dataModel;
            DataId = dataId;
            State = state;
            Error = error;
            Progress = progress;
        }
        public Guid Id { get; set; }
        public string DataModel { get; set; }
        public Guid? DataId { get; set; }
        public string State { get; set; }
        public Guid? Error { get; set; }
        public StateProgress Progress { get; set; }
    }

    public class RsmTableInformation
    {
        public Guid Id { get; set; }
        public string Namespace { get; set; }
        public string Model { get; set; }
        public string Property { get; set; }
        public string TableName { get; set; }
        public DateTime Generated { get; set; }
        public string Version { get; set; }
    }

    public enum StateProgress
    {
        Entering = 0,
        Entered = 1,
        Working = 2,
        Exiting = 3,
        Exited = 4
    }
   
}
