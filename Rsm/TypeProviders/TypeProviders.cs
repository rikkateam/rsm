﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rsm.TypeProviders
{
    /// <summary>
    /// контейнер для базовых моделей
    /// </summary>
    public class BaseModelsContainer
    {
        public BaseModelsContainer()
        {
            Models = new Dictionary<string, DataModel>();

            var baseModel = new DataModel("Базовая модель","baseModel");
            baseModel.Properties.Add(new DataProperty("Идентификатор","id","Uuid", required : true,index:true,unique:true));
            baseModel.Properties.Add(new DataProperty("Дата создания","creation","DateTime"));
            baseModel.Properties.Add(new DataProperty("Дата модификации","modification", "DateTime"));
            baseModel.Properties.Add(new DataProperty("Метка удаления", "deleting", "Boolean"));
            Models.Add(baseModel.SystemName, baseModel);

            var versioning = new DataModel("Базовая версионная модель","baseVersioningModel",rootType:"baseModel");
            versioning.Properties.Add(new DataProperty("Версия","version","String", required: true));
            Models.Add(versioning.SystemName,versioning);

            var dublinCore = new DataModel("Модель дублинского ядра","dublinModel", rootType: "baseModel");
            dublinCore.Properties.Add(new DataProperty("Название","title","String",index:true,multiple:true));
            dublinCore.Properties.Add(new DataProperty("Создатель","creator", "String", index:true, multiple: true));
            dublinCore.Properties.Add(new DataProperty("Тема", "subject", "String", index:true, multiple: true));
            dublinCore.Properties.Add(new DataProperty("Описание", "description", "String", index:true, multiple: true));
            dublinCore.Properties.Add(new DataProperty("Издатель", "publisher", "String", index:true, multiple: true));
            dublinCore.Properties.Add(new DataProperty("Дата", "date", "String", index:true, multiple: true));
            dublinCore.Properties.Add(new DataProperty("Тип", "type", "String", index:true, multiple: true));
            dublinCore.Properties.Add(new DataProperty("Формат", "format", "String", index:true, multiple: true));
            dublinCore.Properties.Add(new DataProperty("Идентификатор", "identifier", "String", index:true, multiple: true));
            dublinCore.Properties.Add(new DataProperty("Источник", "source", "String", index:true, multiple: true));
            dublinCore.Properties.Add(new DataProperty("Язык", "language", "String", index:true, multiple: true));
            dublinCore.Properties.Add(new DataProperty("Отношения", "relation", "String", index:true, multiple: true));
            dublinCore.Properties.Add(new DataProperty("Покрытие", "coverage", "String", index:true, multiple: true));
            dublinCore.Properties.Add(new DataProperty("Права", "rights", "String", index:true, multiple: true));
            Models.Add(dublinCore.SystemName,dublinCore);
        }
        public Dictionary<string,DataModel> Models { get; set; }
    }

    /// <summary>
    /// контейнер специальных простых типов
    /// </summary>
    public class SpecialBasciTypeProviderContainer
    {
        public SpecialBasciTypeProviderContainer()
        {
            TypeProviders = new Dictionary<string, ISpecialBasciTypeProvider>()
            {
                {"Tag", new TagTypeProvider()}
            };
        }
        public Dictionary<string, ISpecialBasciTypeProvider> TypeProviders { get; set; }
    }

    /// <summary>
    /// контейнер для специальных ссылочных типов
    /// </summary>
    public class SpecialRelationTypeProviderContainer
    {
        public SpecialRelationTypeProviderContainer()
        {
            TypeProviders = new Dictionary<string, ISpecialRelationTypeProvider>()
            {
                {"File", new FileTypeRelationProvider()}
            };
        }
        public Dictionary<string, ISpecialRelationTypeProvider> TypeProviders { get; set; }
    }

    /// <summary>
    /// специальный простой тип, подразумевает данне, которые могут быть записаны в одно поле 
    /// </summary>
    public interface ISpecialBasciTypeProvider
    {
        /// <summary>
        /// имя типа
        /// </summary>
        string Type { get; }

        /// <summary>
        /// тип который представляет объект этого типа в .net
        /// </summary>
        Type ManagedType { get; }

        /// <summary>
        /// обычный тип в который записываются данные
        /// </summary>
        string SqlType { get; }

        /// <summary>
        /// метод с помощью которого объект с типом указанным в <see cref="Type"/> записывается в тип который указан в <see cref="SqlType"/>
        /// </summary>
        /// <param name="obj">записываемый объект</param>
        /// <returns></returns>
        string WriteValue(object obj);

        /// <summary>
        /// метод с помощью которого объект считанный из базы считывается в указанный тип <see cref="Type"/>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        object ReadValue(object obj);
    }

    /// <summary>
    /// спеиальный простой тип. сдела в качесте примера. тег, тег просто должен содержать в начале символ решетки
    /// </summary>
    public class TagTypeProvider: ISpecialBasciTypeProvider
    {
        public string Type { get; } = "Tag";
        public Type ManagedType { get; } = typeof (string);
        public string SqlType { get; } = "String";


        public string WriteValue(object obj)
        {
            var tag = (string)obj;
            if (tag != null && !tag.StartsWith("#"))
                tag = "#" + tag;
            return tag;
        }

        public object ReadValue(object obj)
        {
            var tag = (string)obj;
            if (tag != null && !tag.StartsWith("#"))
                tag = "#" + tag;
            return tag;
        }
    }

    /// <summary>
    /// специальный ссылочный тип, хранит данные в другой таблице, а в модели хранится указатель на идентификатор строки
    /// </summary>
    public interface ISpecialRelationTypeProvider
    {
        /// <summary>
        /// имя типа
        /// </summary>
        string Type { get; }

        /// <summary>
        /// тип представленный в .net
        /// </summary>
        Type DestinationType { get; }

        /// <summary>
        /// тип записываемый в модели
        /// </summary>
        string SourceSqlType { get; }
    }

    /// <summary>
    /// специальный ссылочный тип. файлы
    /// </summary>
    public class FileTypeRelationProvider : ISpecialRelationTypeProvider
    {
        public FileTypeRelationProvider()
        {
            Type = "File";
            DestinationType = typeof(FileTypeDataContainer);
            SourceSqlType = "Uuid";
        }

        /// <summary>
        /// имя типа
        /// </summary>
        public string Type { get;}

        /// <summary>
        /// тип представленный в .net
        /// </summary>
        public Type DestinationType { get; }

        /// <summary>
        /// тип записываемый в модели
        /// </summary>
        public string SourceSqlType { get; }
    }

    /// <summary>
    /// Класс описывающий таблицу для <see cref="FileTypeRelationProvider"/>
    /// </summary>
    public class FileTypeDataContainer
    {
        /// <summary>
        /// имя файла
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// mime-тип
        /// </summary>
        public string MimeType { get; set; }

        /// <summary>
        /// версия файла -- возможнобудет реализовано
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// владелец файла  -- возможнобудет реализовано
        /// </summary>
        public string Owner { get; set; }

        /// <summary>
        /// MD5 хэш
        /// </summary>
        public string Md5 { get; set; }

        /// <summary>
        /// идентификатор
        /// </summary>
        public Guid Id { get; set; }
    }
}
