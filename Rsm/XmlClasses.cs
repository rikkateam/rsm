﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Rsm
{
    public static class SerializationHelper<T> where T : class,new()
    {
        public static T Deserilize(Stream stream)
        {
            var ser = new XmlSerializer(typeof(T));
            var result = ser.Deserialize(stream) as T;
            return result;
        }

        public static T Deserilize(string str)
        {
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(str));
            return Deserilize(stream);
        }

        public static Stream Serialize(T data)
        {
            var ser = new XmlSerializer(typeof(T));
            var stream = new MemoryStream();
            ser.Serialize(stream, data);
            stream.Seek(0, 0);
            return stream;
        }

        public static Stream Serialize(T data,Stream stream)
        {
            var ser = new XmlSerializer(typeof(T));
            ser.Serialize(stream, data);
            stream.Seek(0, 0);
            return stream;
        }
        public static string SerializeToString(T data)
        {
            var stream = Serialize(data) as MemoryStream;
            if (stream == null)
                new Exception();
            return Encoding.UTF8.GetString(stream.GetBuffer());
        }
    }
    [XmlRoot]
    public class RsmNamespace
    {
        /// <summary>
        /// Человеко-понятное название состояния (короткое)
        /// </summary>
        [XmlElement]
        public string Name { get; set; }

        /// <summary>
        /// Системное название без специальных символов и пробелов
        /// </summary>
        [XmlAttribute]
        public string SystemName { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        [XmlElement]
        public string Description { get; set; }

        /// <summary>
        /// Комментарий для администратора
        /// </summary>
        [XmlElement]
        public string Comment { get; set; }

        [XmlArray("Processes")]
        [XmlArrayItem("Process")]
        public List<RsmProccess> Proccesses { get; set; }

        [XmlArray("DataModels")]
        [XmlArrayItem("DataModel")]
        public List<DataModel> DataModels { get; set; }

        [XmlArray("Roles")]
        [XmlArrayItem("Role")]
        public List<SmRoles> Roles { get; set; }

        [XmlElement]
        public string Schema { get; set; }

        [XmlElement]
        public string Prefix { get; set; }
    }

    /// <summary>
    /// Класс для описания процесса
    /// </summary>
    [XmlType]
    public class RsmProccess
    {
        /// <summary>
        /// Человеко-понятное название состояния (короткое)
        /// </summary>
        [XmlElement]
        public string Name { get; set; }

        /// <summary>
        /// Системное название без специальных символов и пробелов
        /// </summary>
        [XmlAttribute]
        public string SystemName { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        [XmlElement]
        public string Description { get; set; }

        /// <summary>
        /// Комментарий для администратора
        /// </summary>
        [XmlElement]
        public string Comment { get; set; }

        public RsmProccess()
        {
        }

        [XmlArray("States")]
        [XmlArrayItem("State")]
        public List<ProccessState> States { get; set; }

        [XmlAttribute]
        public string StartState { get; set; }

        [XmlArray("UiDefinitions")]
        [XmlArrayItem("UserInterface")]
        public List<UserInterface> UiDefinitions { get; set; }

        [XmlArray("Permissions")]
        [XmlArrayItem("Pemission")]
        public List<UserPermission> Permissions { get; set; }
    }

    /// <summary>
    /// Класс для описания состояния
    /// </summary>    
    [XmlType]
    public class ProccessState
    {
        /// <summary>
        /// Человеко-понятное название состояния (короткое)
        /// </summary>
        [XmlElement]
        public string Name { get; set; }

        /// <summary>
        /// Системное название без специальных символов и пробелов
        /// </summary>
        [XmlAttribute]
        public string SystemName { get; set; }

        [XmlAttribute]
        public string DataModel { get; set; }

        [XmlAttribute]
        public string ProccessType { get; set; }

        /// <summary>
        /// Скрипт который запускается при входе в статус
        /// </summary>       
        [XmlElement]
        public string EnterScript { get; set; }

        /// <summary>
        /// Скрипт который запускается при выходе из статуса
        /// </summary>
        [XmlElement]
        public string LeaveScript { get; set; }

        /// <summary>
        /// Скрипты запускаемые при изменении модеил
        /// </summary>
        [XmlArray("UpdateScripts")]
        [XmlArrayItem("UpdateScript")]
        public List<ModelUpdateScript> UpdateScripts { get; set; }

        /// <summary>
        /// Действия доступные пользователю
        /// </summary>
        [XmlArray("UserActions")]
        [XmlArrayItem("Action")]
        public List<StateAction> UserActions { get; set; }

        /// <summary>
        /// Прямые переходы на другие состояние доступные пользователю
        /// </summary>
        [XmlArray("JumpActions")]
        [XmlArrayItem("Jump")]
        public List<JumpAction> JumpActions { get; set; }

        /// <summary>
        /// Автоматические условные переходы
        /// </summary>
        [XmlArray("CondTransitions")]
        [XmlArrayItem("Transition")]
        public List<CondTransition> CondTransitions { get; set; }
    }

    /// <summary>
    /// Класс для описания скриптов подписывающихся на изменение значения свойства модели
    /// </summary>
    [XmlType]
    public class ModelUpdateScript
    {
        /// <summary>
        /// Имя свойства или пустота для любого изменения
        /// </summary>
        [XmlElement]
        public string Property { get; set; }

        /// <summary>
        /// Запускаемый скрипт
        /// </summary>
        [XmlElement]
        public string ActionScript { get; set; }
    }

    /// <summary>
    /// Класс для описания действий доступных пользователю
    /// </summary>
    [XmlType]
    public class StateAction
    {
        /// <summary>
        /// Отображаемое название действия (короткое)
        /// </summary>
        [XmlElement]
        public string Text { get; set; }

        /// <summary>
        /// Описание действия
        /// </summary>
        [XmlElement]
        public string Description { get; set; }

        /// <summary>
        /// Условие определяющее доступность действия
        /// </summary>
        [XmlElement]
        public string EnabledCond { get; set; }

        /// <summary>
        /// Запускаемое действие
        /// </summary>
        [XmlElement]
        public string ActionScript { get; set; }
    }

    [XmlType]
    public class CondTransition
    {
        [XmlElement]
        public string ToState { get; set; }
        
        public CondGroup Group { get; set; }

        [XmlArray("HiCond")]
        [XmlArrayItem("Cond")]
        public List<CondElem> HiCond { get; set; }
    }

    [XmlType]
    public class CondElem
    {
        public string Param1 { get; set; }

        public CondOperator Operator { get; set; }

        public string Param2 { get; set; }

        [XmlElement]
        public string Cond { get; set; }
    }

    public enum CondGroup
    {
        And, Or
    }

    public enum CondOperator
    {
        Empty,
        NoEmpty,
        Equally,
        NoEqually,
        More,
        Less,
        GreaterEqual,
        LessEqual,
        Contains,
        NoContains
    }

    /// <summary>
    /// Класс для описания прямого перехода на состояние доступное пользователю
    /// </summary>
    [XmlType]
    public class JumpAction
    {
        /// <summary>
        /// Целевое состояние
        /// </summary>
        [XmlElement]
        public string ToState { get; set; }

        /// <summary>
        /// Условие определяющее доступность прыжка
        /// </summary>
        [XmlElement]
        public string EnabledCond { get; set; }
    }

    /// <summary>
    /// Класс для описания модели данных
    /// </summary>
    [XmlType]
    public class DataModel
    {
        public DataModel()
        {
            RuntimeProperties = new List<DataProperty>();
        }
        public DataModel(string name,string systemName,string rootType=null)
        {
            Name = name;
            SystemName = systemName;
            Properties = new List<DataProperty>();
            RuntimeProperties = new List<DataProperty>();
            RootType = rootType;
        }


        /// <summary>
        /// Человеко-понятное имя (короткое)
        /// </summary>
        [XmlElement]
        public string Name { get; set; }

        /// <summary>
        /// Системное имя
        /// </summary>
        [XmlAttribute]
        public string SystemName { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        [XmlElement]
        public string Description { get; set; }

        /// <summary>
        /// Наследуемый тип
        /// </summary>
        [XmlElement]
        public string RootType { get; set; }

        /// <summary>
        /// Свойства
        /// </summary>
        [XmlArray("Properties")]
        [XmlArrayItem("Property")]
        public List<DataProperty> Properties { get; set; }

        [XmlIgnore]
        public List<DataProperty> RuntimeProperties { get; set; }

        /// <summary>
        /// SQL скрипт создания таблицы
        /// </summary>
        [XmlElement]
        public string TableSqlScript { get; set; }

        /// <summary>
        /// SQL скрипт вставки новой строки
        /// </summary>
        [XmlElement]
        public string InsertSqlScript { get; set; }

        /// <summary>
        /// SQL скрипт изменения данных
        /// </summary>
        [XmlElement]
        public string UpdateSqlScript { get; set; }

        /// <summary>
        /// SQL скрипт выборки данных
        /// </summary>
        [XmlElement]
        public string SelectSqlScript { get; set; }


        ///// <summary>
        ///// SQL скрипт предварительной выборки
        ///// </summary>
        //[XmlElement]
        //public string SelectAllLazySqlScript { get; set; }


        [XmlElement]
        public string TableName { get; set; }

        public string Versioon { get; set; }
    }

    /// <summary>
    /// Класс для описания свойства
    /// </summary>
    [XmlType]
    public class DataProperty
    {
        public DataProperty()
        {
            
        }

        public DataProperty(string name, string systemName, string type,bool required=false,bool index=false,bool unique=false, bool multiple=false,bool cascade=false)
        {
            Name = name;
            SystemName = systemName;
            Type = type;
            Required = required;
            Index = index;
            Unique = unique;
            Multiple = multiple;
            Cascade = cascade;
        }
        /// <summary>
        /// Название
        /// </summary>
        [XmlElement]
        public string Name { get; set; }

        /// <summary>
        /// Системное имя
        /// </summary>
        [XmlAttribute]
        public string SystemName { get; set; }
        

        /// <summary>
        /// Описание
        /// </summary>
        [XmlElement]
        public string Description { get; set; }

        /// <summary>
        /// Комментарий для администратора
        /// </summary>
        [XmlElement]
        public string Comment { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        [XmlAttribute]
        public string Type { get; set; }


        /// <summary>
        /// Имя ссылки с БД
        /// </summary>
        [XmlAttribute]
        public string TypeRelationName { get; set; }

        [XmlAttribute]
        public bool Required { get; set; }

       [XmlAttribute]
        public bool Index { get; set; }

        /// <summary>
        /// Имя индекса в БД
        /// </summary>
        [XmlAttribute]
        public string IndexName { get; set; }

        [XmlAttribute]
        public bool Unique { get; set; }

        /// <summary>
        /// Имя ограничения в БД
        /// </summary>
        [XmlAttribute]
        public string UniqueName { get; set; }

        [XmlAttribute]
        public bool Multiple { get; set; }

        /// <summary>
        /// Имя отноения в БД
        /// </summary>
        [XmlAttribute]
        public string MultipleRelationName { get; set; }


        /// <summary>
        /// Имя отноения в БД
        /// </summary>
        [XmlAttribute]
        public string MultipleTableName { get; set; }

        [XmlAttribute]
        public bool Cascade { get; set; }

        [XmlElement]
        public string DefaultValue { get; set; }


        /// <summary>
        /// Имя ограничения в БД
        /// </summary>
        [XmlAttribute]
        public string DefaultValueName { get; set; }
    }

    [XmlType]
    public class UserInterface
    {
        [XmlElement]
        public string Form { get; set; }
        [XmlElement]
        public string ForState { get; set; }
        [XmlElement]
        public string Controller { get; set; }

        [XmlElement]
        public bool AutoGenerate { get; set; }
    }

    [XmlType]
    public class UserPermission
    {
        [XmlElement]
        public string ForState { get; set; }
        [XmlElement]
        public string Role { get; set; }
        [XmlArray("Actions")]
        [XmlArrayItem("Action")]
        public List<UserPermissionAction> Actions { get; set; }

        [XmlArray("Properties")]
        [XmlArrayItem("Property")]
        public List<UserPermissionProperty> Properties { get; set; }
    }

    [XmlType]
    public class UserPermissionAction
    {
        [XmlElement]
        public string Action { get; set; }
        [XmlElement]
        public bool Allow { get; set; }
        [XmlElement]
        public bool Disallow { get; set; }
    }
    [XmlType]
    public class UserPermissionProperty
    {
        [XmlElement]
        public string Property { get; set; }
        [XmlElement]
        public bool Allow { get; set; }
        [XmlElement]
        public bool Disallow { get; set; }
    }
    public class UserPermissionJump
    {
        [XmlElement]
        public string ToState { get; set; }
        [XmlElement]
        public bool Allow { get; set; }
        [XmlElement]
        public bool Disallow { get; set; }
    }

    [XmlType]
    public class SmRoles
    {
        [XmlElement]
        public string Name { get; set; }
        [XmlElement]
        public string Description { get; set; }
    }
}
