﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rsm;

namespace SeedData
{
    public class Data
    {
        public static RsmNamespace GetRsmNs()
        {
            var ns = new RsmNamespace()
            {
                SystemName = "yama",
                Comment = "Example",
                DataModels = new List<DataModel>(),
                Description = "Example",
                Name = "Yama ns",
                Proccesses = new List<RsmProccess>(),
                Roles = new List<SmRoles>(),
                Schema = "yam",
                Prefix = "xg3h"
            };

            var simple = new DataModel("simple", "simple", rootType: "baseModel");
            simple.Properties.Add(new DataProperty("Name", "name", "String (250)"));
            simple.Properties.Add(new DataProperty("Value", "description", "String"));
            ns.DataModels.Add(simple);

            var thing = new DataModel("thing", "thing", rootType: "baseModel");
            thing.Properties.Add(new DataProperty("Name", "name", "String (250)", index: true, unique: true, required: true));
            thing.Properties.Add(new DataProperty("Description", "description", "String"));
            thing.Properties.Add(new DataProperty("Property", "property", "String", multiple: true, cascade: true));
            thing.Properties.Add(new DataProperty("Count", "count", "Int"));
            ns.DataModels.Add(thing);


            var incoming = new DataModel("Incoming", "incoming", rootType: "dublinModel") {TableName = ns.Prefix+'_'+"Incoming" };
            incoming.Properties.Add(new DataProperty("Метод доставки", "deliveryMethod", "String"));
            incoming.Properties.Add(new DataProperty("Номер", "registerNumber", "String"));
            incoming.Properties.Add(new DataProperty("Дата регистрации", "registerDate", "DateTime"));
            incoming.Properties.Add(new DataProperty("Состояние", "state", "Int"));
            ns.DataModels.Add(incoming);


            var answer = new DataModel("Answer", "answer", rootType: "dublinModel");
            answer.Properties.Add(new DataProperty("В ответ на", "answerTo", "incoming", cascade: true));
            answer.Properties.Add(new DataProperty("Связи вх", "relncoming", "incoming", multiple: true, cascade: true));
            answer.Properties.Add(new DataProperty("Метод доставки", "deliveryMethod", "String"));
            answer.Properties.Add(new DataProperty("Номер", "registerNumber", "String"));
            answer.Properties.Add(new DataProperty("Дата регистрации", "registerDate", "DateTime"));
            answer.Properties.Add(new DataProperty("Состояние", "state", "Int"));
            ns.DataModels.Add(answer);

            using (var file = File.Create("yama.xml"))
            {
                SerializationHelper<RsmNamespace>.Serialize(ns, file);
            }
            return ns;
        }
    }
}
