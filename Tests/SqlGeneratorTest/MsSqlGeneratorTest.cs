﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using Dapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rsm;
using Rsm.Generators;
using Rsm.TypeProviders;

namespace SqlGeneratorTest
{
    [TestClass]
    public class MsSqlGeneratorTest
    {
        public MsSqlGeneratorTest()
        {

        }


        /// <summary>
        /// </summary>
        [TestMethod]
        public void GenerateTables()
        {
            var ns = SeedData.Data.GetRsmNs();
            var engine = new RsmEngine(ns);
            var simple = engine.Generator.TableCreate("simple");
            var thing = engine.Generator.TableCreate("thing");
            var incoming = engine.Generator.TableCreate("incoming");
            var answer = engine.Generator.TableCreate("answer");
            File.WriteAllText("simple.sql", simple);
            File.WriteAllText("thing.sql", thing);
            File.WriteAllText("incoming.sql", incoming);
            File.WriteAllText("answer.sql", answer);
            engine.Connection.Open();
            using (var transaction = engine.Connection.BeginTransaction())
            {
                engine.Connection.Execute(simple,transaction: transaction);
                engine.Connection.Execute(thing, transaction: transaction);
                engine.Connection.Execute(incoming, transaction: transaction);
                engine.Connection.Execute(answer, transaction: transaction);
                transaction.Commit();
            }

            var info = SerializationHelper<RsmNamespace>.SerializeToString(ns);
            File.WriteAllText("GenerateTables.xml", info);
        }

        [TestMethod]
        public void Inseret1()
        {
            var ns = SerializationHelper<RsmNamespace>.Deserilize(File.ReadAllText("GenerateTables.xml"));
            var engine = new RsmEngine(ns);
            var model = engine.Models["simple"];
            var item = new Dictionary<string, object>()
            {
                {"Name","Router" },
                {"Value","1" },
            };
            var transact = Guid.NewGuid().ToString("N");
            var id = engine.InsertData("thing", item, transact);
            engine.Commit(transact);
        }
        [TestMethod]
        public void Inseret2()
        {
            var ns = SerializationHelper<RsmNamespace>.Deserilize(File.ReadAllText("GenerateTables.xml"));
            var engine = new RsmEngine(ns);
            var model = engine.Models["thing"];
            var item = new Dictionary<string, object>()
            {
                {"Name","Router" },
                {"Count",1 },
                {"Property",new [] {"square","white","electrical"} }
            };
            var transact = Guid.NewGuid().ToString("N");
            var id = engine.InsertData("thing", item, transact);
            engine.Commit(transact);
        }
    }
}
